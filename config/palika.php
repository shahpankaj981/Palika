<?php
return [
    'municipalityName'             => 'Nagarpalika',
    'maintenanceContact'           => 'mka@citizeninfotech.com.np',
    'appApiKey'                    => 'AAAA4soRnAI:APA91bGHkaOwQjRHfJlYX90xWsbIY26-_m3ro-HRucsGUaMWer1nAD-NthfTPkVdCCANNWWAkKYBw9_CU0Ye48F9Bqg02FVznUW01kH98IA-6P6oO_Mp_ahZ9jpaBBO0go9hDrig-_5i',
    'androidTopic'                 => 'com.nabinbhandari.municipality',
    'siteEmail'                    => 'admin@findandfill.com',
    'galleryCategoriesPerPageData' => 2,
    'galleryImagesPageData'        => 2,
    'fileCategoriesPerPageData'    => 2,
    'uploadedFilesPageData'        => 2,
    'name_title'                   => [
        '1' => 'Mr.',
        '2' => 'Ms.',
        '3' => 'Mrs.',
        '4' => 'Miss',
    ],
    'boolean_option'               => [
        '0' => 'No',
        '1' => "Yes",
    ],
    'socialMedias'                 => [
        1 => 'facebook',
        2 => 'google',
        3 => 'twitter',
    ],
    'userStatus'                   => [
        ['id' => 1, 'name' => 'Active'],
        ['id' => 2, 'name' => 'Inactive'],
        ['id' => 3, 'name' => 'Payment Error'],
        ['id' => 4, 'name' => 'Removed'],
    ],
    'galleryCategory'              => [
        ['name' => 'Miscellaneous'],
    ],
    'importantContactCategory'     => [
        ['name' => 'Miscellaneous'],
    ],
];
