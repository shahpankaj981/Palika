# Palika

A governmental Web application using Laravel following Repository design pattern that is used to manage the data of a municipality of Nepal and also serves as the backedn api for the mobile app.


## Install

This application can be cloned from gitlab repository and installed. Following the procedure given below:

* git clone `git@git.ebpearls.com:php/find_fill.git`
* cd find_fill

## Run

The app can be run with the command below:

* install the application dependencies using command: `composer install`
* Copy .env file `cp .env.example .env`
* Update database and email configuration

## Working & Deploying
* Follow the concept of simplified gitflow.

## Framework

The application is written in PHP based on the [Laravel](http://laravel.com) framework, current version of Laravel 
used for this project is 5.2.
 
## Running Tests

Tests can be run through command `./vendor/bin/phpunit`

## Check code quality

We follow [PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md) for 
coding standard  

## Coding Conventions

We have followed PSR-2 coding convention.
